#!/bin/sh
# set -e

install()
{
    #$1: Version
    #$2: Base instalation folder
    TEMP_PATH=$(create_temp_dir `echo $2`)
    logit "Temp path: $TEMP_PATH"
    cd $TEMP_PATH
    download $1
    logit "Descompactando pacote."
    tar -xf nudoku-*.tar.xz
    logit "Pacote descompactado."
    rm -f nudoku-*.tar.xz
    cd `ls`
    remove
    logit "Instalando nudoku"
    complile_and_install
    logit "Nudoku instalado"
    logit "Removendo diretório temporário"
    remove_temp_dir $2
    logit ">>>>>>>>>>>>>>>>>>>>>Instalação concluída<<<<<<<<<<<<<<<<<<<<<<<<"
}

 complile_and_install(){
     ./configure > /dev/null 2>&1
     make install > /dev/null 2>&1
 }

 remove(){
    logit "Removendo nudoku"
    NUDOKU_FILE=`which nudoku` 
    if [ -f $NUDOKU_FILE ]
    then
        rm -f $NUDOKU_FILE
    fi
    MAN_PATH=`manpath -g`
    for path in ${MAN_PATH//:/ }; do
        if [ -f $path/nudoku* ]; then
            rm -fv $path/nudoku*
            logit "Apagado: $path/nudoku*"
        fi
    done
    logit "Nudoku removido com sucesso!"
}

download(){
    #$1: Version
    VERSION="1.0.0"
    if [ ! -z $1  ] #is not empty
    then
        VERSION=$1
    fi
    URL="https://github.com/jubalh/nudoku/releases/download/1.0.0/nudoku-$VERSION.tar.xz"
    logit "Downloading nudoku $VERSION ($URL)"
    wget $URL > /dev/null 2>&1
    logit "Download concluído!"
}

create_temp_dir(){
    #$1: Path where will be create the temp folder.
    TMP_PATH="$1/tmp-install-files"
    if [ -d $TMP_PATH ];
    then
        rm -rf $TMP_PATH
        remove_temp_dir $1
    fi
    mkdir $TMP_PATH
    echo $TMP_PATH
}

remove_temp_dir(){
    #$1: Path where will be create the temp folder.
    TMP_PATH="$1/tmp-install-files"
    if [ -d $TMP_PATH ];
    then
        rm -rf $TMP_PATH
    fi
}

logit() 
{
    echo "[${USER}][`date`] - ${*}" >> ${LOG_FILE}
    echo "[${USER}][`date`] - ${*}"
}

################ MAIN ############
BASE_DIR=`pwd`
LOG_FILE="$BASE_DIR/install-nudoku.log" 
logit "Base Folder: $BASE_DIR"
case $1 in
    "instalar") install $2 $BASE_DIR $LOG_FILE
    ;;
    "remover") remove
    ;;
    *) logit "Parâmetro desconhecido"
    ;;
esac
